import { https } from "./url.config";

export const movieService = {
  getBanner: () => {
    let uri = "api/QuanLyPhim/LayDanhSachBanner";
    return https.get(uri);
  },

  getMovie: (params) => {
    let uri = "api/QuanLyPhim/LayDanhSachPhimPhanTrang";
    return https.get(uri, { params: params });
  },

  getListMovie: (params) => {
    let uri = "/api/QuanLyPhim/LayDanhSachPhim";
    return https.get(uri, { params });
  },

  getTheater: (params) => {
    let uri = "/api/QuanLyRap/LayThongTinHeThongRap";
    return https.get(uri, { params });
  },

  getMovieByTheater: (params) => {
    let uri = "/api/QuanLyRap/LayThongTinLichChieuHeThongRap";
    return https.get(uri, { params });
  },

  getMovieDetail: (params) => {
    let uri = "/api/QuanLyPhim/LayThongTinPhim";
    return https.get(uri, { params });
  },

  getTimeMovieInfo: (params) => {
    let uri = "/api/QuanLyRap/LayThongTinLichChieuPhim";
    return https.get(uri, { params });
  },

  getInfoTicket: (params) => {
    let uri = "/api/QuanLyDatVe/LayDanhSachPhongVe";
    return https.get(uri, { params });
  },

  postBookingTicket: (data) => {
    let uri = "/api/QuanLyDatVe/DatVe";
    return https.post(uri, data);
  },

  postBookedTicket: (data) => {
    let uri = "/api/QuanLyDatVe/DatVe";
    return https.post(uri, data);
  },
};
