import axios from "axios";
import { store } from "../index.js";
import {
  TURN_OFF_LOADING,
  TURN_ON_LOADING,
} from "../redux/constants/spinnerConstants";

export const TOKEN_CYBER =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzM0UiLCJIZXRIYW5TdHJpbmciOiIyNi8wNC8yMDIzIiwiSGV0SGFuVGltZSI6IjE2ODI0NjcyMDAwMDAiLCJuYmYiOjE2NTQzNjIwMDAsImV4cCI6MTY4MjYxNDgwMH0.GGqFf8-ZXIqAjnaJZ40LjQvUHb1VyvRv3XtEIsMe_qE";
export const BASE_URL = "https://movienew.cybersoft.edu.vn";

export const configHeaders = () => {
  return {
    TokenCybersoft: TOKEN_CYBER,
  };
};

export const MA_NHOM = "GP07";

export const https = axios.create({
  baseURL: BASE_URL,
  headers: configHeaders(),
});

// Add a request interceptor
https.interceptors.request.use(
  function (config) {
    store.dispatch({
      type: TURN_ON_LOADING,
    });

    // turn on loading
    // Do something before request is sent
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
https.interceptors.response.use(
  function (response) {
    store.dispatch({
      type: TURN_OFF_LOADING,
    });

    // turn off loading
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  }
);
