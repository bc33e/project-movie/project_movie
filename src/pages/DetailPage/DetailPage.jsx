import { Button } from "antd";
import React, { useEffect } from "react";
import { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Footer from "../../components/Footer/Footer";
import Header from "../../components/Header/Header";
import { movieService } from "../../service/movie.service";
import DetailPageTime from "./DetailPageTime";

export default function DetailPage() {
  const navigate = useNavigate();
  const { id } = useParams();
  const [movieDetail, setMovieDetail] = useState({});
  const [movieTime, setMovieTime] = useState({});
  let fetchMovieDetail = async ({ id }) => {
    let params = {
      maPhim: id,
    };
    try {
      let res = await movieService.getMovieDetail(params);
      setMovieDetail(res.data.content);
      let resMovieTime = await movieService.getTimeMovieInfo(params);
      setMovieTime(resMovieTime.data.content);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    fetchMovieDetail({ id });
  }, []);

  const [readMore, setReadMore] = useState(true);
  let toogleReadMore = () => {
    setReadMore(!readMore);
  };

  let readMoreInfo = (content) => {
    return (
      <p className="text">
        {readMore ? content.slice(0, 300) : content}
        <span onClick={toogleReadMore} className="cursor-pointer text-white">
          {readMore ? "... xem thêm" : " Ẩn bớt"}
        </span>
      </p>
    );
  };
  return (
    <div>
      <Header />
      <div
        style={{
          background:
            "linear-gradient(to right,rgba(0,0,0,1)150px,rgba(0,0,0,.8)100%)",
        }}
      >
        <div className="xl:flex md:flex xl:container md:container xl:space-y-0 md:space-y-0 space-y-8 xl:px-10 px-5 xl:mx-auto md:mx-auto xl:py-12 md:py-12 py-10">
          <div className="xl:w-1/4 md:w-1/4 w-2/5 relative">
            <img
              className="object-cover mx-auto rounded"
              src={movieDetail.hinhAnh}
              alt=""
            />
          </div>
          <div className="xl:w-3/4 md:w-3/4 w-full xl:pl-16 md:pl-12 xl:space-y-10 md:space-y-7 space-y-5">
            <div className="xl:space-y-3 md:space-y-1 space-y-1">
              <p className="flex items-start xl:space-x-3 md:space-x-2 space-x-2 xl:text-3xl md:text-2xl text-xl font-bold text-white">
                <span className="bg-red-600 xl:text-lg md:text-base text-sm text-white xl:px-1 md:px-1 px-1 rounded-sm">
                  C18
                </span>
                <span>{movieDetail.tenPhim}</span>
              </p>
              <p className="text-white xl:text-base md:text-sm">
                {movieDetail.tenPhim} ~ 120 phút
              </p>
            </div>
            <div className="xl:space-y-3 md:last:space-y-2">
              <p className="xl:text-xl md:text-lg text-lg text-white font-bold">
                NỘI DUNG PHIM
              </p>
              <p className="text-white xl:text-base md:text-sm">
                {movieDetail.moTa?.length > 300
                  ? readMoreInfo(movieDetail.moTa)
                  : movieDetail.moTa}
              </p>
            </div>
            <div>
              <div className="xl:block md:block flex items-center justify-center">
                <button
                  // onClick={handelClickScrollMuaVe}
                  className="xl:px-3 md:px-2 px-3 xl:py-2 md:py-1 py-2 xl:text-lg md:text-lg text-base font-bold bg-blue-700 hover:bg-blue-800 text-white hover:text-white duration-200 rounded"
                >
                  Mua vé
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <DetailPageTime
        danhGia={movieDetail.danhGia}
        tenPhim={movieDetail.tenPhim}
        hinhAnh={movieDetail.hinhAnh}
        movieTime={movieTime}
        maPhim={id}
      />
      <Footer />
    </div>
  );
}
