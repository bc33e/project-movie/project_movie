import { Collapse, Tabs, Rate, Progress } from "antd";
import React from "react";
import styled from "styled-components";
import ButtonBooking from "../../components/BookingTicket/ButtonBooking";
import Lottie from "lottie-react";
import bgWaiting from "../../asset/bg_waiting.json";

const { Panel } = Collapse;
const CustomTabs = styled(Tabs)`
  .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn {
    color: #1e40af !important;
  }
  .ant-tabs-tab:hover {
    color: #1e70af !important;
  }
  .ant-tabs-ink-bar {
    position: absolute;
    background: #1e40af;
    pointer-events: none;
  }
`;

export default function DetailPageTime(props) {
  let renderMovieTimeTable = (lichChieuPhim, tenCumRap, diaChiCumRap) => {
    return lichChieuPhim.map((lichChieu) => {
      return (
        <ButtonBooking
          key={lichChieu.maLichChieu}
          maLichChieu={lichChieu.maLichChieu}
          ngayChieuGioChieu={lichChieu.ngayChieuGioChieu}
          tenCumRap={tenCumRap}
          diaChiCumRap={diaChiCumRap}
          tenPhim={props.movieTime.tenPhim}
          hinhAnh={props.hinhAnh}
          maPhim={props.maPhim}
        />
      );
    });
  };

  let renderTheater = (cumRapChieu, logo) => {
    return cumRapChieu?.map((cumRap) => {
      return (
        <Panel
          header={
            <div className="xl:flex md:flex flex space-x-3 xl:justify-around md:justify-around w-full">
              <div className="xl:w-1/12 md:w-1/12 w-1/12 flex items-center">
                <img src={logo} alt="" />
              </div>
              <div className="xl:w-10/12 md:w-10/12 w-11/12 text-left border-b">
                <div className="xl:text-xl md:text-xl text-lg">
                  {cumRap.tenCumRap}
                </div>
                <div className="flex items-center justify-between text-sm space-x-2">
                  <span className="xl:text-base md:text-base text-xs">
                    {cumRap.diaChi}
                  </span>
                </div>
              </div>
            </div>
          }
          key={cumRap.maCumRap}
        >
          <div className="h-full overflow-auto">
            {renderMovieTimeTable(
              cumRap.lichChieuPhim,
              cumRap.tenCumRap,
              cumRap.diaChi
            )}
          </div>
        </Panel>
      );
    });
  };

  let renderContent = () => {
    if (props.movieTime.heThongRapChieu.length !== 0) {
      return props.movieTime.heThongRapChieu?.map((heThongRap) => {
        return (
          <Tabs.TabPane
            tab={
              <div className=" xl:p-2 md:p-2 p-2 border border-gray-400 rounded-lg ">
                <img
                  className="xl:w-8 md:w-8 w-6  xl:h-8 md:h-8 h-6"
                  src={heThongRap.logo}
                />
              </div>
            }
            key={heThongRap.maHeThongRap}
          >
            <Collapse
              accordion
              expandIconPosition="end"
              className="xl:h-[28rem] md:h-96"
              defaultActiveKey="1"
            >
              {renderTheater(heThongRap.cumRapChieu, heThongRap.logo)}
            </Collapse>
          </Tabs.TabPane>
        );
      });
    } else {
      return (
        <div className="container text-center text-xl font-semibold mb-3">
          <div className="w-1/3 mx-auto">
            <Lottie animationData={bgWaiting} />
            Hiện tại phim này chưa có lịch chiếu!
          </div>
        </div>
      );
    }
  };
  return (
    <div className="xl:container xl:mx-auto py-8 px-3">
      <h1 className="mb-5  text-xl font-bold xl:text-left md:text-left text-center">
        Lịch chiếu: {props.movieTime.tenPhim}
      </h1>
      <div>
        {props.movieTime.dangChieu ? (
          <div className="xl:flex md:flex xl:pb-10 xl:space-x-4 md:space-x-4   border ">
            <div className="xl:w-2/3 md:w-2/3">
              <CustomTabs
                tabBarGutter={"2rem"}
                tabBarExtraContent={{
                  left: <div className="xl:w-8 md:w-4 w-4"></div>,
                }}
                tabPosition="top"
                defaultActiveKey="BHDStar"
              >
                {renderContent()}
              </CustomTabs>
            </div>

            <div className="xl:w-1/3 md:w-1/3 w-full xl:flex md:flex  flex  justify-center items-center">
              <div className=" font-bold text-center  xl:mt-20 mt-10 py-4 rounded shadow-lg border ">
                <div>
                  Đánh giá của phim:{" "}
                  <div className="mt-5">
                    <Progress
                      className="text-2xl"
                      format={(number) => {
                        return number / 10;
                      }}
                      strokeColor={"green"}
                      type="circle"
                      percent={props.danhGia * 10}
                    />
                    <br />
                    <Rate disabled allowHalf defaultValue={props.danhGia / 2} />
                  </div>
                  <p className="xl:mt-2 md:mt-2 mt-2">
                    {props.danhGia * 10}% người xem hài lòng với phim này
                  </p>
                  <div className="xl:py-6 md:py-5 py-4"></div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="container text-center  text-xl font-semibold mb-3">
            <div className="w-1/3 mx-auto">
              <Lottie animationData={bgWaiting} />
              Phim sẽ sớm ra mắt trong thời gian tới !!!
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
