import React from "react";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { userInfoLocal } from "../../service/local.service";
import Lottie from "lottie-react";
import Bg_Animate from "../../asset/bg_login.json";
import { Button, Form, Input, message } from "antd";
import { setLoginActionService } from "../../redux/actions/userAction";
import backgroundPage from "../../asset/img/background.jpg";

export default function LoginPage() {
  let dispatch = useDispatch();
  let navigate = useNavigate();

  useEffect(() => {
    let userInfo = userInfoLocal.get();
    if (userInfo) {
      navigate("/");
    }
  }, []);

  const onFinish = (dataUser) => {
    let handleSuccess = () => {
      message.success("Đăng nhập thành công");
      setTimeout(() => {
        navigate("/");
      }, 500);
    };
    dispatch(setLoginActionService(dataUser, handleSuccess));
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div
      style={{
        backgroundImage: `url(${backgroundPage})`,
        backgroundSize: "150%",
        backgroundPosition: "center",
      }}
      className="h-screen w-screen flex justify-center items-center px-20"
    >
      <div className="xl:flex md:flex flex items-center justify-center xl:w-2/3 md:w-2/3 w-3/4  xl:pr-16 md:pr-10 xl:pb-20 md:pb-12 pb-10  xl:pt-12 md:pt-7 pt-10 bg-[rgba(0,0,0,.75)] z-10 text-white">
        <div className="w-1/2 transform scale-50">
          <Lottie animationData={Bg_Animate} />
        </div>
        <div className="w-1/2 flex items-center justify-center">
          <Form
            className="w-full"
            layout="vertical"
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Tài khoản"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập tài khoản!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Mật khẩu"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập mật khẩu!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 0,
                span: 24,
              }}
            >
              <button
                className="xl:w-full md:w-full w-full  xl:text-lg md:text-base text-sm  xl:px-4 md:px-2 px-2 xl:py-4 md:py-2 py-1 text-white hover:text-white bg-blue-500  hover:bg-blue-700  transition rounded-xl"
                htmlType="submit"
              >
                Đăng nhập
              </button>
              <div className=" text-right  xl:px-5 md:px-5 px-0 xl:text-base md:text-base text-xs text-white">
                Bạn chưa có tài khoản?{" "}
                <span
                  onClick={() => {
                    navigate("/register");
                  }}
                  className="text-blue-400 hover:text-blue-600 font-medium cursor-pointer duration-150"
                >
                  Hãy đăng ký ngay
                </span>
              </div>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
