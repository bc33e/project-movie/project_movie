import React from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { userService } from "../../service/user.service";
import Bg_Animate from "../../asset/bg_login.json";
import { Form, Input, message } from "antd";
import Lottie from "lottie-react";
import InputForm from "../../components/Form/InputForm";
import backgroundPage from "../../asset/img/background.jpg";
import logo from "../../asset/img/logo.png";

export default function RegisterPage() {
  const navigate = useNavigate();
  const [inputValue, setInputValue] = useState({
    taiKhoan: "",
    matKhau: "",
    email: "",
    soDt: "",
    hoTen: "",
  });
  const [errInputValue, setErrInputValue] = useState({
    taiKhoan: "",
    matKhau: "",
    email: "",
    soDt: "",
    hoTen: "",
  });

  let handleChangeInputValue = (event) => {
    let { value, name } = event.target;
    let checkRegex = (regex, message) => {
      if (regex.test(value)) {
        setErrInputValue({ ...errInputValue, [name]: "" });
      } else {
        setErrInputValue({ ...errInputValue, [name]: message });
      }
    };

    if (value.trim() == "") {
      setErrInputValue({
        ...errInputValue,
        [name]: ` * Vui lòng điền thông tin`,
      });
    } else {
      setErrInputValue({
        ...errInputValue,
        [name]: ``,
      });
    }
    if (value.length >= 1) {
      switch (name) {
        case "taiKhoan":
          {
            checkRegex(
              /^[a-zA-Z0-9_]*$/,
              "Không được chứa khoảng trắng và ký tự đặc biệt"
            );
          }
          break;
        case "matKhau":
          {
            checkRegex(
              /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
              "Tối thiểu 8 ký tự, gồm ít nhất một chữ cái và một số, không chứa kí tự đặc biệt"
            );
          }
          break;
        case "email":
          {
            checkRegex(
              /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
              "Email chưa đúng định dạng"
            );
          }
          break;
        case "soDt":
          {
            checkRegex(
              /(84|0[3|5|7|8|9])+([0-9]{8})+$\b/,
              "Vui lòng nhập đúng số điện thoại"
            );
          }
          break;
        case "hoTen": {
          checkRegex(
            /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/,
            "Họ tên phải là chữ"
          );
        }
      }
    }
    setInputValue({ ...inputValue, [name]: value });
  };

  let handleSubmit = (event) => {
    event.preventDefault();
    let isValid = true;
    for (let key in inputValue) {
      if (inputValue[key] == "") {
        isValid = false;
      }
    }
    for (let key in errInputValue) {
      if (errInputValue[key] != "") {
        isValid = false;
      }
    }
    // Kiểm tra từ sever
    if (isValid) {
      let handleRegisterUser = async (user) => {
        try {
          let res = await userService.postRegister(user);
          console.log("res: ", res);

          message.success("Đăng kí thành công");
          navigate("/login");
        } catch (err) {
          if (err.response.data.content == "Email đã tồn tại!") {
            setErrInputValue({ ...errInputValue, email: "Email đã tồn tại!" });
          } else {
            setErrInputValue({
              ...errInputValue,
              taiKhoan: "Tài khoản đã tồn tại!",
            });
          }
          message.error("Đăng Kí thất bại");
        }
      };
      handleRegisterUser(inputValue);
    } else {
      message.error("Đăng kí thất bại");
    }
  };

  return (
    <div
      style={{
        backgroundImage: `url(${backgroundPage})`,
        backgroundSize: "150%",
        backgroundPosition: "center",
      }}
      className="h-screen w-screen flex justify-center items-center px-20"
    >
      <div
        onClick={() => {
          setTimeout(() => {
            navigate("/");
          }, 500);
        }}
        className="absolute top-5 left-5 text-5xl text-white hover:text-blue-700 font-extrabold cursor-pointer opacity-70 z-50"
      >
        <img src={logo} style={{ height: 50, width: 75 }} alt="" />
      </div>
      <div className="flex items-center justify-center w-full h-full">
        <form className="flex items-center justify-center xl:w-1/2 md:w-1/2 w-4/5 md:px-8  xl:py-8 py-8 md:py-12  bg-[#191919bf] z-10">
          <div className="xl:w-2/3  md:w-full w-2/3 xl:space-y-2 md:space-y-2 space-y-2">
            <h1 className="xl:text-4xl md:text-3xl text-2xl  xl:mb-10 md:mb-7 mb-6 text-white text-center">
              ĐĂNG KÝ THÔNG TIN
            </h1>
            <InputForm
              name="taiKhoan"
              label="Tài khoản"
              inputValue={inputValue.taiKhoan}
              errInputValue={errInputValue.taiKhoan}
              handleChangeInputValue={handleChangeInputValue}
            />
            <InputForm
              name="matKhau"
              label="Password"
              inputValue={inputValue.matKhau}
              errInputValue={errInputValue.matKhau}
              handleChangeInputValue={handleChangeInputValue}
              type="password"
            />
            <InputForm
              name="email"
              label="Email"
              inputValue={inputValue.email}
              errInputValue={errInputValue.email}
              handleChangeInputValue={handleChangeInputValue}
            />
            <InputForm
              name="soDt"
              label="Điện thoại"
              inputValue={inputValue.soDt}
              errInputValue={errInputValue.soDt}
              handleChangeInputValue={handleChangeInputValue}
            />
            <InputForm
              name="hoTen"
              label="Họ tên"
              inputValue={inputValue.hoTen}
              errInputValue={errInputValue.hoTen}
              handleChangeInputValue={handleChangeInputValue}
            />
            <button
              type="submit"
              onClick={(e) => {
                handleSubmit(e);
              }}
              className=" xl:w-full md:w-full w-full xl:text-lg md:text-lg text-lg xl:px-4 md:px-4 px-3 xl:py-4 md:py-4 py-2 text-white hover:text-white bg-blue-700 hover:bg-blue-800  transition rounded-xl"
            >
              Đăng ký
            </button>
            <div className="text-right  xl:px-5 md:px-5 px-0 xl:text-base md:text-base text-xs text-white">
              Bạn đã có tài khoản?{" "}
              <span
                onClick={() => {
                  navigate("/login");
                }}
                className="text-blue-400 hover:text-blue-600 font-medium cursor-pointer duration-150"
              >
                Xin hãy đăng nhập
              </span>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
