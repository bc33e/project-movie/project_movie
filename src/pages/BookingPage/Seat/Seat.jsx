import React from "react";
import { useDispatch, useSelector } from "react-redux";
import style from "./seat.module.css";
import { handleChooseSeat } from "../../../redux/actions/seatAction";

export default function Seat({ seats }) {
  const dispatch = useDispatch();
  let arrSeat = useSelector((state) => state.seatReducer.arrSeat);

  let renderSeat = () => {
    return seats.slice(0, 100).map((ghe) => {
      let seatStyle = "";
      let disable = false;

      //chọn ghế
      if (ghe.loaiGhe === "Thuong") {
        seatStyle = `${style.seat} ${style.regular}`;
      } else {
        seatStyle = `${style.seat} ${style.vip}`;
      }

      // ghế đã đặt
      if (ghe.daDat) {
        seatStyle = `${style.seat} ${style.booked}`;
        disable = true;
      }

      // ghế đang chọn
      let index = arrSeat.findIndex((bookingSeat) => {
        return bookingSeat.maGhe == ghe.maGhe;
      });

      if (index !== -1) {
        seatStyle = `${style.seat} ${style.booking}`;
      }
      return (
        <button
          disabled={disable}
          onClick={() => {
            dispatch(handleChooseSeat(ghe));
          }}
          key={ghe.maGhe}
          className={`${seatStyle}`}
        >
          {ghe.tenGhe}
        </button>
      );
    });
  };
  return (
    <div className="w-full xl:w-2/3">
      <div
        className={`w-4/5 mx-auto mt-3 ${style.screen} bg-yellow-600 shadow-2xl flex items-end justify-center`}
      >
        <span className="text-black">Màn hình</span>
      </div>
      <div className="w-full  xl:w-4/5 mx-auto mt-4">{renderSeat()}</div>
      <hr className="my-4 mx-auto w-4/5 h-1 bg-gray-100 rounded border-0  dark:bg-gray-700" />
      <div className="w-4/5  xl:flex justify-center items-center mx-auto">
        <div className="w-full xl:w-1/2 mx-auto mb-4 flex justify-start items-center space-x-2">
          <div className="w-1/2">
            <button className={`${style.seat} ${style.regular}`}>x</button>
            <span className="text-white">Ghế thường</span>
          </div>
          <div className="w-1/2">
            <button className={`${style.seat} ${style.vip}`}>x</button>
            <span className="text-white">Ghế Vip</span>
          </div>
        </div>
        <div className="w-full xl:w-1/2 mx-auto mb-4 flex justify-start items-center space-x-2">
          <div className="w-1/2">
            <button className={`${style.seat} ${style.booking}`}>x</button>
            <span className="text-white">Ghế đang đặt</span>
          </div>
          <div className="w-1/2">
            <button className={`${style.seat} ${style.booked}`}>x</button>
            <span className="text-white">Ghế đã đặt</span>
          </div>
        </div>
      </div>
    </div>
  );
}
