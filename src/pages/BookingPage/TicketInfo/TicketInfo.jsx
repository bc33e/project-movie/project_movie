import { Button, Descriptions } from "antd";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { movieService } from "../../../service/movie.service";
import { handleCancelSeat } from "../../../redux/actions/seatAction";

export default function TicketInfo({ ticketInfo }) {
  console.log("ticketInfo: ", ticketInfo);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  let isLoading = useSelector((state) => {
    return state.spinnerReducer.isLoading;
  });

  let arrSeat = useSelector((state) => {
    return state.seatReducer.arrSeat;
  });

  let userInfo = useSelector((state) => {
    return state.userReducer.userInfo;
  });

  let renderBookingTicket = () => {
    return arrSeat?.map((ghe) => {
      return (
        <span key={ghe.maGhe} className="text-blue-500">
          {ghe.tenGhe}
        </span>
      );
    });
  };

  let handleBuyTicket = () => {
    let maLichChieu = ticketInfo.maLichChieu;
    let danhSachVe = arrSeat.map((ghe) => {
      return {
        maGhe: ghe.maGhe,
        giaVe: ghe.giaVe,
      };
    });
    let bookingInfo = {
      maLichChieu: maLichChieu,
      danhSachVe: danhSachVe,
    };

    let sendBookingInfo = async (data) => {
      try {
        let res = await movieService.postBookingTicket(data);
        console.log("res: ", res);
      } catch (err) {
        console.log("err: ", err);
      }
    };
    sendBookingInfo(bookingInfo);
    setTimeout(() => {
      Swal.fire({
        icon: "success",
        title: "Đặt vé thành công",
        showConfirmButton: false,
        timer: 2000,
      });
    }, 700);
  };

  return (
    <div className="w-full xl:w-1/3">
      <div className="mt-3 flex items-center justify-center bg-[rgba(0,0,0,.75)] text-white">
        <div className="col-span-3">
          <h1 className="text-4xl mt-8">THÔNG TIN ĐẶT VÉ</h1>
          <hr />
          <div className="flex justify-between mt-3">
            <h3>Cụm rạp: </h3>
            <h3 className="text-blue-400">{ticketInfo.tenCumRap}</h3>
          </div>
          <div className="flex justify-between mt-3">
            <h3>Rạp: </h3>
            <h3 className="text-blue-400">{ticketInfo.tenRap}</h3>
          </div>
          <div className="flex justify-between mt-3">
            <h3>Địa chỉ: </h3>
            <h3 className="text-blue-400">{ticketInfo.diaChi}</h3>
          </div>
          <div className="flex justify-between mt-3">
            <h3>Tên phim: </h3>
            <h3 className="text-blue-400">{ticketInfo.tenPhim}</h3>
          </div>
          <div className="flex justify-between mt-3">
            <h3>Ngày, giờ chiếu: </h3>
            <h3 className="text-blue-400">
              {ticketInfo.ngayChieu} ~ {ticketInfo.gioChieu}
            </h3>
          </div>
          <div className="flex justify-between mt-3 mb-3">
            <h3>Ghế chọn</h3>
            <article>{renderBookingTicket()}</article>
          </div>
          <hr />

          <div className="mt-3 flex justify-between items-center w-full">
            <h3>Tên Khách hàng: {""}</h3>
            <span className="text-white">
              {userInfo ? userInfo.hoTen : " "}
            </span>
          </div>
          <div className="flex justify-between mt-3">
            <h3>Tổng số tiền: </h3>
            <h3 className="text-white">
              {arrSeat
                ?.reduce((tongSoTien, ghe) => (tongSoTien += ghe.giaVe), 0)
                .toLocaleString()}{" "}
              VND
            </h3>
          </div>
          <div>
            {arrSeat.length === 0 ? (
              <button className="mt-3 xl:w-full md:w-full w-full  xl:text-lg md:text-base text-sm  xl:px-4 md:px-2 px-2 xl:py-4 md:py-2 py-1 text-white hover:text-white bg-blue-500  hover:bg-blue-700 transition rounded-xl">
                Xác nhận
              </button>
            ) : (
              <button
                className="mt-3 xl:w-full md:w-full w-full  xl:text-lg md:text-base text-sm  xl:px-4 md:px-2 px-2 xl:py-4 md:py-2 py-1 text-white hover:text-white bg-blue-500  hover:bg-blue-700 transition rounded-xl"
                onClick={() => {
                  handleBuyTicket();
                  dispatch(handleCancelSeat());
                  navigate("/");
                }}
              >
                Xác nhận
              </button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
