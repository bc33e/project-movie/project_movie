import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import { movieService } from "../../service/movie.service";
import backgroundPage from "../../asset/img/background.jpg";
import Header from "../../components/Header/Header";
import Seat from "./Seat/Seat";
import TicketInfo from "./TicketInfo/TicketInfo";

import Footer from "../../components/Footer/Footer";

export default function BookingPage() {
  const navigate = useNavigate();
  const [seats, setSeats] = useState([]);
  const [ticketInfo, setTicketInfo] = useState([]);
  const { id } = useParams();

  let userInfo = useSelector((state) => state.userReducer.userInfo);
  let fetchBooking = async ({ id }) => {
    let params = {
      maLichChieu: id,
    };
    try {
      let res = await movieService.getInfoTicket(params);
      setSeats(res.data.content.danhSachGhe);
      setTicketInfo(res.data.content.thongTinPhim);
    } catch (err) {
      console.log("err: ", err);
    }
  };

  useEffect(() => {
    fetchBooking({ id });
  });

  useEffect(() => {
    console.log("Thông tin user: ", userInfo);
    if (!userInfo) {
      navigate("/");
      setTimeout(() => {
        Swal.fire({
          icon: "error",
          title: "Vui lòng đăng nhập để được mua vé",
          showConfirmButton: false,
          timer: 2000,
        });
      }, 500);
    }
  }, [userInfo]);
  return (
    <div
      style={{
        backgroundImage: `url(${backgroundPage})`,
        backgroundSize: "150%",
        backgroundPosition: "center",
      }}
    >
      <Header />
      <div className="xl:flex items-center w-full">
        <Seat seats={seats} />
        <TicketInfo ticketInfo={ticketInfo} />
      </div>
      <Footer />
    </div>
  );
}
