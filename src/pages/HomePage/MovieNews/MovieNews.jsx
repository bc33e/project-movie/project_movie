import { Tabs } from "antd";
import React from "react";
import MovieNewsItem from "./MovieNewsItem";
import { newsData } from "./MovieNewsData";
import styled from "styled-components";

const MovieTabs = styled(Tabs)`
  .ant-tabs-ink-bar {
    position: absolute;
    background: #1d4ed8;
    pointer-events: none;
  }
`;

export default function MovieNews() {
  return (
    <div className="container mx-auto xl:px-20 md:px-10 px-3 pt-20 pb-16">
      <MovieTabs tabBarGutter={50} centered defaultActiveKey="1">
        <Tabs.TabPane
          tab={
            <div className="xl:text-xl md:text-xl text-base  text-black font-bold">
              Điện ảnh 24h
            </div>
          }
          key="1"
          centered
          defaultActiveKey="1"
        >
          {<MovieNewsItem dataNews={newsData} />}
        </Tabs.TabPane>
        <Tabs.TabPane
          tab={
            <div className="xl:text-xl md:text-xl text-base  text-black font-bold">
              Review
            </div>
          }
          key="2"
        >
          {<MovieNewsItem dataNews={newsData} />}
        </Tabs.TabPane>
        <Tabs.TabPane
          tab={
            <div className="xl:text-xl md:text-xl text-base  text-black font-bold">
              Khuyến mãi
            </div>
          }
          key="3"
        >
          {<MovieNewsItem dataNews={newsData} />}
        </Tabs.TabPane>
      </MovieTabs>
    </div>
  );
}
