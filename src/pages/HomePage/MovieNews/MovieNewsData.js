export const newsData = [
  {
    id: 1,
    img: "https://www.cgv.vn/media/banner/cache/1/b58515f018eb873dafa430b6f9ae0c1e/t/r/tro_t_n_r_c_r__980wx448h.jpg",
    content:
      "'Tro Tàn Rực Rỡ' là câu chuyện về tình yêu đầy nhân văn của những người phụ nữ miền Tây dành cho người đàn ông nhiều tổn thương của họ.",
    title:
      "Dựa trên hai truyện ngắn 'Tro Tàn Rực Rỡ' và 'Củi Mục Trôi Về' của nhà văn Nguyễn Ngọc Tư.",
  },

  {
    id: 2,
    img: "https://www.cgv.vn/media/banner/cache/1/b58515f018eb873dafa430b6f9ae0c1e/9/8/980_x_448__10.jpg",
    content:
      "Khi một nhóm lính đánh thuê tấn công một gia đình giàu có, ông già Noel phải bước vào để cứu họ (và lễ Giáng sinh).",
    title: "Khi một nhóm lính đánh thuê tấn công một gia đình giàu có ...",
  },
  {
    id: 3,
    img: "https://www.cgv.vn/media/banner/cache/1/b58515f018eb873dafa430b6f9ae0c1e/l/a/late_shift_-_rolling_banner_980x448.jpg",
    content:
      "Matt, một học sinh thông minh với việc làm thêm buổi tối tại một bãi đậu xe hạng sang, phải chứng minh sự trong sạch của mình khi bị buộc phải tham gia vào một vụ trộm táo bạo tại nhà đấu giá nổi tiếng London.",
    title:
      "Một cuộc hành trình trốn chạy xuyên thành phố khỏi những tên truy sát diễn ra ngay trong đêm.",
  },
  {
    id: 4,
    img: "https://www.cgv.vn/media/catalog/product/cache/1/image/1800x/71252117777b696995f01934522c402d/h/p/hpmposter_w1024xh768_1_1_.jpg",
    content:
      "Người mẹ vì lề thói gia trưởng mà trọng nam khinh nữ, sẵn sàng vì con trai mà không từ thủ đoạn nào. Dù phải trả giá cho những hành động của mình...",
    title: "Bộ phim kể về bi kịch của dòng họ Vương Đình.",
  },
  {
    id: 5,
    img: "https://www.cgv.vn/media/catalog/product/cache/1/image/1800x/71252117777b696995f01934522c402d/r/s/rsz_980wx448h_op.jpg",
    content:
      "Luffy và Uta lần đầu tiên hội ngộ sau lần gặp gỡ vào 12 năm trước tại Làng Foosha.",
    title: "Bối cảnh One Piece Film Red diễn ra ở hòn đảo âm nhạc Elegia...",
  },
  {
    id: 6,
    img: "https://www.cgv.vn/media/catalog/product/cache/1/image/1800x/71252117777b696995f01934522c402d/b/l/black-panther-2-poster-1.jpg",
    content:
      "Khi người Wakanda cố gắng nắm bắt chương tiếp theo của họ, các anh hùng phải hợp tác với nhau...",
    title:
      "Nữ hoàng Ramonda cùng mọi người chiến đấu để bảo vệ quốc gia của họ khỏi sự can thiệp của các thế lực thế giới sau cái chết của Đức Vua.",
  },
  {
    id: 7,
    img: "https://ss-images.saostar.vn/wp700/pc/1668763025866/saostar-z5p45ijoasfmx08d.png",
    content:
      "Tại nơi được gọi là phòng bệnh, các 'Anne' bị tiêm thuốc và thôi miên mỗi ngày để 'chữa trị' chứng hoang tưởng và giúp họ nhớ ra mình là ai",
    title: "10 cô gái, 10 nhân diện, thức dậy tại một nơi xa lạ...",
  },
  {
    id: 8,
    img: "https://www.cgv.vn/media/catalog/product/cache/1/image/1800x/71252117777b696995f01934522c402d/h/p/hp6.jpg",
    content:
      "Harry Potter tình cờ khám phá được cuốn sách của Hoàng Tử Lai, từ đó phát hiện ra nhiều bí mật liên quan đến Chúa Tể Hắc Ám",
    title:
      "Khi các Tử thần Thực tử tàn sát thế giới phù thủy và Muggle, Hogwarts không còn là nơi trú ẩn an toàn cho học sinh...",
  },
];
