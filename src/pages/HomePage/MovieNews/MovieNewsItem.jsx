import React from "react";
import movieItem from "./movieNews.css";

export default function MovieNewsItem({ dataNews }) {
  return (
    <div className="xl:grid md:grid xl:grid-cols-6 md:grid-cols-2 gap-10 xl:px-20 py-6">
      <div className="xl:col-span-3 md:h-96 h-96 xl:h-96">
        <div className="xl:h-2/3 md:h-2/5 h-2/5">
          <img
            style={{ height: "100%", width: "100%" }}
            className="movieItem h-full w-full object-cover"
            src={dataNews[0].img}
            alt=""
          />{" "}
        </div>
        <div className="font-medium text-xl mt-2 cursor-pointer hover:text-blue-500">
          {dataNews[0].title.substr(0, 50) + " ..."}
        </div>
        <p className="mt-3">{dataNews[0].content.substr(0, 100) + " ..."}</p>
      </div>
      <div className="xl:col-span-3 md:h-96 h-96 xl:h-96">
        <div className="xl:h-2/3 md:h-2/5 h-2/5">
          <img
            className="movieItem h-full w-full object-cover"
            src={dataNews[1].img}
            alt=""
          />{" "}
        </div>
        <div className="font-medium text-xl mt-2 cursor-pointer hover:text-blue-500">
          {dataNews[1].title.substr(0, 50) + " ..."}
        </div>
        <p className="mt-3">{dataNews[1].content.substr(0, 100) + " ..."}</p>
      </div>
      <div className="xl:col-span-2 md:h-96 h-96 xl:h-96">
        <div className="xl:h-2/3 md:h-2/5 h-2/5">
          <img
            className="movieItem h-full w-full object-cover"
            src={dataNews[2].img}
            alt=""
          />{" "}
        </div>
        <div className="font-medium text-xl mt-2 cursor-pointer hover:text-blue-500">
          {dataNews[2].title.substr(0, 50) + " ..."}
        </div>
        <p className="mt-3">{dataNews[2].content.substr(0, 100) + " ..."}</p>
      </div>
      <div className="xl:col-span-2 md:h-96 h-96 xl:h-96">
        <div className="xl:h-2/3 md:h-2/5 h-2/5">
          <img
            className="movieItem h-full w-full object-cover"
            src={dataNews[3].img}
            alt=""
          />{" "}
        </div>
        <div className="font-medium text-xl mt-2 cursor-pointer hover:text-blue-500">
          {dataNews[3].title.substr(0, 50) + " ..."}
        </div>
        <p className="mt-3">{dataNews[3].content.substr(0, 100) + " ..."}</p>
      </div>
      <div className="xl:col-span-2 xl:block md:hidden hidden md:h-52  xl:h-96 p-3 space-y-4">
        <div className="flex space-x-2">
          <img
            className="movieItemSm"
            width={70}
            height={70}
            src={dataNews[4].img}
            alt=""
          />
          <span className="font-medium cursor-pointer hover:text-blue-500">
            {dataNews[4].title.substr(0, 50) + " ..."}
          </span>
        </div>
        <div className="flex space-x-2">
          <img
            className="movieItemSm"
            width={70}
            height={70}
            src={dataNews[5].img}
            alt=""
          />
          <span className=" font-medium cursor-pointer hover:text-blue-500">
            {dataNews[5].title.substr(0, 50) + " ..."}
          </span>
        </div>
        <div className="flex space-x-2">
          <img
            className="movieItemSm"
            width={70}
            height={70}
            src={dataNews[6].img}
            alt=""
          />
          <span className=" font-medium cursor-pointer hover:text-blue-500">
            {dataNews[6].title.substr(0, 50) + " ..."}
          </span>
        </div>
        <div className="flex space-x-2">
          <img
            className="movieItemSm"
            width={70}
            height={70}
            src={dataNews[7].img}
            alt=""
          />
          <span className=" font-medium cursor-pointer hover:text-blue-500">
            {dataNews[7].title.substr(0, 50) + " ..."}
          </span>
        </div>
      </div>
    </div>
  );
}
