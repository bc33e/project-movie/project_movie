import React from "react";
import Footer from "../../components/Footer/Footer";
import Header from "../../components/Header/Header";
import HomePageCarousel from "./Carousel/HomePageCarousel";
import MovieList from "./MovieList/MovieList";
import MovieNews from "./MovieNews/MovieNews";
import MovieTab from "./MovieTab/MovieTab";
import SearchMovie from "./SearchMovie/SearchMovie";

export default function HomePage() {
  return (
    <div>
      <Header />
      <HomePageCarousel />
      {/* <SearchMovie /> */}
      <MovieList />
      <h1 className="xl:text-4xl md:text-4xl text-3xl text-center text-blue-700 font-bold mb-8">
        Lịch chiếu phim
      </h1>
      <MovieTab />
      <h1 className="xl:text-4xl md:text-4xl text-3xl text-center text-blue-700 font-bold mt-5">
        Tin tức
      </h1>
      <MovieNews />
      <Footer />
    </div>
  );
}
