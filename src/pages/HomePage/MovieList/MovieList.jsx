import React, { useEffect, useRef, useState } from "react";
import { movieService } from "../../../service/movie.service";
import MovieItem from "./MovieItem";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import backGroundPage from "../../../asset/img/background.jpg";
import { CaretLeftOutlined, CaretRightOutlined } from "@ant-design/icons";
import Slider from "react-slick";
import { MA_NHOM } from "../../../service/url.config";

export default function MovieList() {
  const [movieList, setMovieList] = useState({});
  const fetchMovies = async () => {
    const params = {
      maNhom: MA_NHOM,
    };
    try {
      const res = await movieService.getMovie(params);
      console.log("Danh sách phim", res.data.content);
      setMovieList(res.data.content);
    } catch (err) {
      console.log("Load ListMovie thất bại:  ", err);
    }
  };

  useEffect(() => {
    fetchMovies({});
  }, []);

  const refSlide = useRef();
  const nextSlide = () => {
    refSlide.current.slickNext();
  };
  const prevSlide = () => {
    refSlide.current.slickPrev();
  };
  const settingSlide = {
    className: "h-full",
    // dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 5,
    arrows: false,
    initialSlide: 0,
  };

  let renderMovieList = () => {
    return movieList.items?.map((item, index) => {
      return <MovieItem key={item.maPhim} movieData={item} index={index} />;
    });
  };
  return (
    <div
      className="xl:py-12 md:py-10 py-8 xl:my-12 md:my-12 text-center"
      style={{ backgroundImage: `url(${backGroundPage})` }}
    >
      <h1 className="text-white font-bold text-2xl ">THÔNG TIN PHIM</h1>
      <div className="relative xl:h-[32rem] md:h-[31rem] h-[20rem]  flex items-center justify-center container mx-auto">
        <button
          onClick={prevSlide}
          className="text-gray-500 hover:text-white xl:text-5xl md:5xl text-3xl absolute xl:left-10 md:left-5 left-2 xl:top-2/5 md:top-1/3 top-1/3"
        >
          <CaretLeftOutlined />
        </button>
        <button
          onClick={nextSlide}
          className="text-gray-500 hover:text-white xl:text-5xl md:5xl text-3xl absolute xl:right-10 md:right-5 right-2 xl:top-2/5 md:top-1/3 top-1/3"
        >
          <CaretRightOutlined />
        </button>
        <div className="h-3/4 w-5/6">
          <Slider ref={refSlide} {...settingSlide}>
            {renderMovieList()}
          </Slider>
        </div>
      </div>
    </div>
  );
}
