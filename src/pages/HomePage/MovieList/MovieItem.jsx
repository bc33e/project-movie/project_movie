import React from "react";
import { useNavigate } from "react-router-dom";

export default function MovieItem({ movieData, index }) {
  const navigate = useNavigate();
  return (
    <div className="p-2 h-96 cursor-pointer">
      <div
        onClick={() => {
          navigate(`/detail/${movieData.maPhim}`);
        }}
        className="xl:h-5/6 md:h-4/6 h-3/6 flex items-center justify-center relative overflow-hidden rounded-xl border border-gray-400"
      >
        <h2 className="xl:text-3xl md:text-2xl text-2xl absolute bottom-0 left-0 font-bold text-gray-400">
          {index + 1}
        </h2>
        <img
          className="object-cover h-full hover:h-[150%] w-[150%] duration-300"
          src={movieData.hinhAnh}
          alt=""
        />
      </div>
      <div className="h-1/6 py-3 text-gray-400 hover:text-white font-bold xl:text-xl md:text-base text-xs ">
        {movieData.tenPhim}
      </div>
    </div>
  );
}
