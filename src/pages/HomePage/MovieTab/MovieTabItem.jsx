import { Tabs } from "antd";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { movieService } from "../../../service/movie.service";
import { MA_NHOM } from "../../../service/url.config";
import MovieTabInfo from "./MovieTabInfo";
import { useMediaQuery } from "react-responsive";

export default function MovieTabItem({ maHeThongRap }) {
  const isDesktop = useMediaQuery({ minWidth: 1024 });
  const [theaters, setTheaters] = useState({});
  let fetchTheater = async ({ maHeThongRap }) => {
    let params = {
      maNhom: MA_NHOM,
      maHeThongRap,
    };
    try {
      let res = await movieService.getMovieByTheater(params);
      setTheaters(res.data.content[0]);
    } catch (err) {
      console.log("err: ", err);
    }
  };

  useEffect(() => {
    fetchTheater({ maHeThongRap });
  }, [maHeThongRap]);

  let renderTheaterInfo = (danhSachPhim, tenCumRap, diaChi) => {
    return danhSachPhim.map((movie) => {
      return (
        <MovieTabInfo
          dataMovie={movie}
          tenCumRap={tenCumRap}
          diaChi={diaChi}
          key={movie.maPhim}
        />
      );
    });
  };

  let renderTheater = () => {
    return theaters.lstCumRap?.map((cumRap) => {
      return (
        <Tabs.TabPane
          style={{ height: "35rem" }}
          tab={
            <div className="w-64  text-center border-b">{cumRap.tenCumRap}</div>
          }
          key={cumRap.maCumRap}
        >
          <div className="h-full overflow-auto">
            {renderTheaterInfo(
              cumRap.danhSachPhim,
              cumRap.tenCumRap,
              cumRap.diaChi
            )}
          </div>
        </Tabs.TabPane>
      );
    });
  };
  return (
    <Tabs
      className=" w-full xl:h-[38rem] md:h-[40rem] h-[40rem]"
      centered
      defaultActiveKey="BHD Star Cineplex - Bitexco"
      tabPosition={isDesktop ? "left" : "top"}
    >
      {renderTheater()}
    </Tabs>
  );
}
