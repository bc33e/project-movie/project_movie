import { Tabs } from "antd";
import React, { useEffect } from "react";
import { useState } from "react";
import { movieService } from "../../../service/movie.service";
import { MA_NHOM } from "../../../service/url.config";
import MovieTabItem from "./MovieTabItem";
import logo from "../../../asset/img/logo.png";

export default function MovieTab() {
  const [movieTab, setMovieTab] = useState([]);
  let fetchMovieTab = async () => {
    let params = {
      maNhom: MA_NHOM,
    };
    try {
      let res = await movieService.getTheater(params);
      setMovieTab(res.data.content);
    } catch (err) {
      console.log("err: ", err);
    }
  };

  useEffect(() => {
    fetchMovieTab();
  }, []);

  let renderMovieTab = () => {
    return movieTab.map((heThongRap, index) => {
      return (
        <Tabs.TabPane
          tab={
            <img
              src={heThongRap.logo}
              className="xl:w-10 xl:h-10 md:h-10 md:w-10 w-10 h-10 md:mx-8"
            />
          }
          key={heThongRap.maHeThongRap}
        >
          <MovieTabItem maHeThongRap={heThongRap.maHeThongRap} />
        </Tabs.TabPane>
      );
    });
  };
  return (
    <div className="container mx-auto border-2 rounded-xl shadow-xl">
      <Tabs
        tabBarExtraContent={{
          left: (
            <div className="hidden xl:w-80 md:hidden xl:block text-right xl:text-5xl md:text-3xl font-extrabold justify-center items-center">
              <img src={logo} style={{ height: 50, width: 75 }} alt="" />
            </div>
          ),
        }}
        tabPosition="top"
        defaultActiveKey="BHDStar"
      >
        {renderMovieTab()}
      </Tabs>
    </div>
  );
}
