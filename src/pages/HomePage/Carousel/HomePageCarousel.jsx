import { Carousel } from "antd";
import React, { useEffect, useState, useRef } from "react";
import { movieService } from "../../../service/movie.service";
import { CaretLeftOutlined, CaretRightOutlined } from "@ant-design/icons";

export default function HomePageCarousel() {
  const [banners, setBanners] = useState([]);
  let ref = useRef();
  const fetchBanner = async () => {
    try {
      const res = await movieService.getBanner();
      setBanners(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchBanner();
  }, []);

  const onChange = (currentSlide) => {
    console.log(currentSlide);
  };
  return (
    <div className="w-full relative">
      <Carousel dots={false} afterChange={onChange} ref={ref}>
        {banners.map((item) => {
          return (
            <div className="w-full" key={item.maBanner}>
              <img
                className="h-100 object-cover w-full"
                src={item.hinhAnh}
                alt=""
              />
            </div>
          );
        })}
      </Carousel>
      <button
        className="text-white absolute left-0 top-1/2"
        onClick={() => {
          ref.current.prev();
        }}
      >
        <CaretLeftOutlined
          className="transition ease-in-out text-gray-500 hover:text-white hover:scale-150 duration-300"
          style={{ fontSize: "3rem" }}
        />
      </button>
      <button
        className="text-white absolute right-0 top-1/2"
        onClick={() => {
          ref.current.next();
        }}
      >
        <CaretRightOutlined
          className="transition ease-in-out text-gray-500 hover:text-white hover:scale-150 duration-300"
          style={{ fontSize: "3rem" }}
        />
      </button>
    </div>
  );
}
