import { Button } from "antd";
import moment from "moment/moment";
import React, { useEffect, useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { movieService } from "../../../service/movie.service";
import { MA_NHOM } from "../../../service/url.config";

export default function SearchMovie() {
  const navigate = useNavigate();

  const [listMovie, setListMovie] = useState([]);
  const [listTheater, setListTheater] = useState([]);
  const [theaterInfo, setTheaterInfo] = useState([]);
  const [theaterValue, setTheaterValue] = useState();
  const selectMovie = useRef();
  const selectTheater = useRef();
  const selectTime = useRef();

  // Gọi API List Phim
  let fetchMovie = async () => {
    let params = {
      maNhom: MA_NHOM,
    };
    try {
      let res = await movieService.getListMovie(params);
      setListMovie(res.data.content);
    } catch (err) {
      console.log("Load ListMovie thất bại: ", err);
    }
  };

  useEffect(() => {
    fetchMovie();
  }, []);

  // Gọi API Time Movie
  let fetchTimeMovie = async (maPhim) => {
    let params = {
      maPhim: maPhim,
    };
    try {
      let res = await movieService.getTimeMovieInfo(params);
      setListTheater(res.data.content.heThongRapChieu);
    } catch (err) {
      console.log("Load ListTheater thất bại: ", err);
    }
  };

  //render list phim
  let renderListMovie = () => {
    return listMovie.map((item) => {
      return (
        <option value={item.maPhim} key={item.maPhim}>
          {item.tenPhim}
        </option>
      );
    });
  };

  // đổi movie
  const [disable, setDisable] = useState(false);
  let handleChangeNameMovie = (event) => {
    let { value } = event.target;
    if (value != 0) {
      fetchTimeMovie(value);
      setDisable(true);
    }
  };

  let compareArray = (arr1, arr2) => {
    return JSON.stringify(arr1) === JSON.stringify(arr2);
  };

  // render rạp
  let renderListTheater = () => {
    let theaterList = [];
    listTheater.forEach((heThongRap) => {
      heThongRap.cumRapChieu.forEach((cumRap) => {
        theaterList.push(cumRap);
      });
    });
    let compareArr = compareArray(theaterList, theaterInfo);
    if (!compareArr) {
      setTheaterInfo(theaterList);
    }
    return theaterList.map((cumRap) => {
      return (
        <option value={cumRap.maCumRap} key={cumRap.maCumRap}>
          {cumRap.tenCumRap}
        </option>
      );
    });
  };

  // đổi rạp

  let handleChangeTheater = (event) => {
    let { value } = event.target;
    setTheaterValue(value);
  };

  // render lịch chiếu

  let renderTimetable = () => {
    let userChooseTheater = theaterInfo.find(
      (cumRap) => cumRap.maCumRap == theaterValue
    );
    return userChooseTheater?.lichChieuPhim.map((lichChieu) => {
      return (
        <option value={lichChieu.maLichChieu} key={lichChieu.maLichChieu}>
          {moment(lichChieu.ngayChieuGioChieu).format("DD/MM/YYYY ~ HH:MM")}
        </option>
      );
    });
  };

  let handleBooking = () => {
    let checkTime = selectTime.current.value;
    let checkTheater = selectTheater.current.value;
    if (checkTheater != 0 && checkTime != 0) {
      navigate(`/booking/${checkTime}`);
    } else {
      Swal.fire({
        icon: "error",
        title: "Vui lòng chọn đủ thông tin",
        showConfirmButton: false,
        timer: 1000,
      });
    }
  };

  return (
    <div className="container mx-auto flex items-center justify-center xl:t-36 md:pt-28 pt-20 pb-10">
      <div className="grid grid-cols-4 xl:w-5/6 md:w-full xl:p-3 md:p-2 border-blue-400 border shadow-xl rounded">
        <div className="w-full">
          <select ref={selectMovie} onChange={handleChangeNameMovie}>
            <option value="0" selected disabled={disable}>
              Chọn tên phim
            </option>
            {renderListMovie()}
          </select>
        </div>
        <div>
          <select ref={selectTheater} onChange={handleChangeTheater}>
            <option value="0">Chọn rạp phim</option>
            {renderListTheater()}
          </select>
        </div>
        <div>
          <select ref={selectTime}>
            <option value="0">Chọn ngày giờ</option>
            {renderTimetable()}
          </select>
        </div>
        <div className="flex items-center justify-center">
          <Button
            onClick={handleBooking()}
            className="bg-blue-700 hover:px-4 px-3 py-2 hover:py-3 duration-150 text-white w-2/3 rounded-xl"
          >
            Mua vé
          </Button>
        </div>
      </div>
    </div>
  );
}
