import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, NavLink } from "react-router-dom";
import { userInfoLocal } from "../../service/local.service";
import { SET_LOGIN } from "../../redux/constants/userConstant";

export default function UserNavbar() {
  let userInfo = useSelector((state) => {
    return state.userReducer.userInfo;
  });
  let navigate = useNavigate();
  let dispatch = useDispatch();

  let handleLogOut = () => {
    //remove data và chuyển trang
    userInfoLocal.remove();
    dispatch({
      type: SET_LOGIN,
      payload: null,
    });
    //laod lại trang
    navigate("/");
  };

  let renderContent = () => {
    if (userInfo) {
      return (
        <div>
          <div>
            <span className="mr-5 text-black hover:text-blue-500 text-lg font-medium cursor-pointer">
              {userInfo.hoTen}
            </span>
            <button
              onClick={handleLogOut}
              className="bg-red-600 hover:bg-red-800 text-white px-5 py-2 rounded shadow-lg"
            >
              Đăng xuất
            </button>
          </div>
        </div>
      );
    } else {
      return (
        <div>
          <NavLink
            to="/login"
            className="bg-green-600 hover:bg-green-800 text-white mr-5 shadow-lg px-5 py-2 rounded"
          >
            Đăng nhập
          </NavLink>
          <NavLink
            to="/register"
            className="bg-blue-600 hover:bg-blue-800 text-white shadow-lg px-5 py-2 rounded"
          >
            Đăng ký
          </NavLink>
        </div>
      );
    }
  };
  return <div>{renderContent()}</div>;
}
