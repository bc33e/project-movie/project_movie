import { Button } from "antd";
import React from "react";
import UserNavbar from "./UserNavbar";
import logo from "../../asset/img/logo.png";
import { useNavigate } from "react-router-dom";

export default function Header() {
  let navigate = useNavigate();
  let style = {};
  return (
    <div
      className="shadow-lg"
      style={{
        transition: "0.5s",
        // background: "rgba(0, 0, 0, 0.9)",
      }}
    >
      <div className="container mx-auto flex justify-between items-center h-20 px-10">
        <div
          className="cursor-pointer"
          onClick={() => {
            {
              navigate("/");
            }
          }}
        >
          <img src={logo} style={{ height: 50, width: 75 }} alt="" />
        </div>
        <div className="space-x-11 font-medium text-black text-lg">
          <span className="hover:text-blue-500">
            <a href="">Lịch chiếu</a>
          </span>
          <span className="hover:text-blue-500">
            <a href="">Cụm rạp</a>
          </span>
          <span className="hover:text-blue-500">
            <a href="">Tin tức</a>
          </span>
          <span className="hover:text-blue-500">
            <a href="">Ứng dụng</a>
          </span>
        </div>
        <UserNavbar />
      </div>
    </div>
  );
}
