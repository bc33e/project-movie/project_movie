import React from "react";
import {
  EnvironmentFilled,
  FacebookFilled,
  GitlabFilled,
  InstagramFilled,
  LinkedinFilled,
  MailFilled,
  PhoneFilled,
  YoutubeFilled,
} from "@ant-design/icons";
import logo from "../../asset/img/logo.png";

export default function Footer() {
  return (
    <div>
      <footer style={{ background: "rgba(0, 0, 0, 0.9)" }}>
        <div className="mx-auto max-w-screen-xl px-4 pt-16 pb-6 sm:px-6 lg:px-8 lg:pt-24">
          <div className="grid grid-cols-1 gap-8 lg:grid-cols-3">
            <div>
              <div className="flex justify-center  items-center sm:justify-start">
                <img src={logo} style={{ height: 50, width: 75 }} alt="" />
              </div>
              <ul className="mt-6 flex justify-center sm:justify-start md:gap-4 text-xl">
                <li>
                  <a
                    href="/"
                    className="text-gray-500 transition hover:text-white"
                  >
                    <FacebookFilled />
                  </a>
                </li>
                <li>
                  <a
                    href="/"
                    className="text-gray-500 transition hover:text-white"
                  >
                    <InstagramFilled />
                  </a>
                </li>
                <li>
                  <a
                    href="/"
                    className="text-gray-500 transition hover:text-white"
                  >
                    <YoutubeFilled />
                  </a>
                </li>
                <li>
                  <a
                    href="/"
                    className="text-gray-500 transition hover:text-white"
                  >
                    <GitlabFilled />
                  </a>
                </li>
                <li>
                  <a
                    href="/"
                    className="text-gray-500 transition hover:text-white"
                  >
                    <LinkedinFilled />
                  </a>
                </li>
              </ul>
            </div>
            <div className="grid grid-cols-1 gap-8 sm:grid-cols-2 md:grid-cols-4 lg:col-span-2">
              <div className="text-center sm:text-left">
                <p className="text-lg font-medium text-blue-500 hover:text-white">
                  Our link
                </p>
                <nav className="mt-8">
                  <ul className="space-y-4 text-sm">
                    <li>
                      <a
                        href="/"
                        className="text-gray-500 font-medium transition hover:text-white font-medium/75"
                      >
                        Home
                      </a>
                    </li>
                    <li>
                      <a
                        href="/login"
                        className="text-gray-500 font-medium transition hover:text-white font-medium/75"
                      >
                        Login
                      </a>
                    </li>
                    <li>
                      <a
                        href="/booking"
                        className="text-gray-500 font-medium transition hover:text-white font-medium/75"
                      >
                        Booking Ticket
                      </a>
                    </li>
                    <li>
                      <a
                        href="/detail"
                        className="text-gray-500 font-medium transition hover:text-white font-medium/75"
                      >
                        Detail
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
              <div className="text-center sm:text-left">
                <p className="text-lg font-medium text-blue-500 hover:text-white">
                  Our Services
                </p>
                <nav className="mt-8">
                  <ul className="space-y-4 text-sm">
                    <li>
                      <a
                        className="text-gray-500 font-medium transition hover:text-white font-medium/75"
                        href="/"
                      >
                        Web Design
                      </a>
                    </li>
                    <li>
                      <a
                        className="text-gray-500 font-medium transition hover:text-white font-medium/75"
                        href="/"
                      >
                        Web Development
                      </a>
                    </li>
                    <li>
                      <a
                        className="text-gray-500 font-medium transition hover:text-white font-medium/75"
                        href="/"
                      >
                        Web Solution
                      </a>
                    </li>
                    <li>
                      <a
                        className="text-gray-500 font-medium transition hover:text-white font-medium/75"
                        href="/"
                      >
                        App Design
                      </a>
                    </li>
                    <li>
                      <a
                        className="text-gray-500 font-medium transition hover:text-white font-medium/75"
                        href="/"
                      >
                        Digital Marketing
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
              <div className="text-center sm:text-left">
                <p className="text-lg font-medium text-blue-500 hover:text-white">
                  Helpful Links
                </p>
                <nav className="mt-8">
                  <ul className="space-y-4 text-sm">
                    <li>
                      <a
                        className="text-gray-500 hover:text-white  font-medium transition font-medium/75"
                        href="/"
                      >
                        FAQs
                      </a>
                    </li>
                    <li>
                      <a
                        className="text-gray-500 hover:text-white  font-medium transition font-medium/75"
                        href="/"
                      >
                        Portfolio
                      </a>
                    </li>
                    <li>
                      <a
                        className="text-gray-500 hover:text-white font-medium transition font-medium/75"
                        href="/"
                      >
                        Terms & Conditions
                      </a>
                    </li>
                    <li>
                      <a
                        className="text-gray-500 hover:text-white font-medium transition font-medium/75"
                        href="/"
                      >
                        Privacy Policy
                      </a>
                    </li>
                    <li>
                      <a
                        className="group flex justify-center gap-1.5 sm:justify-start"
                        href="/"
                      >
                        <span className="text-gray-500 font-medium transition group-hover:text-white font-medium/75">
                          Live Chat
                        </span>
                        <span className="relative -mr-2 flex h-2 w-2">
                          <span className="absolute inline-flex h-full w-full animate-ping rounded-full bg-purple-400 opacity-75" />
                          <span className="relative inline-flex h-2 w-2 rounded-full bg-green-500" />
                        </span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
              <div className="text-center sm:text-left">
                <p className="text-lg font-medium text-blue-500 hover:text-white">
                  Contact Us
                </p>
                <ul className="mt-8">
                  <li>
                    <a
                      href="/"
                      className="flex items-center justify-center gap-1.5 sm:justify-start mb-4"
                    >
                      <MailFilled className="text-gray-500 hover:text-white" />
                      <span className="text-gray-500 font-medium hover:text-white font-medium/75">
                        contact@example.com
                      </span>
                    </a>
                  </li>
                  <li>
                    <a
                      href="/"
                      className="flex items-center justify-center gap-1.5 sm:justify-start mb-4"
                    >
                      <PhoneFilled className="text-gray-500 hover:text-white" />
                      <span className="text-gray-500 font-medium hover:text-white font-medium/75">
                        0987654321
                      </span>
                    </a>
                  </li>
                  <li>
                    <a
                      href="/"
                      className="flex items-center justify-center gap-1.5 sm:justify-start"
                    >
                      <EnvironmentFilled className="text-gray-500 hover:text-white" />
                      <span className="text-gray-500 hover:text-white -mt-0.5 not-italic font-medium transition font-medium/75">
                        Ho Chi Minh, Viet Nam
                      </span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="mt-16 border-t border-gray-100 pt-8">
            <p className="text-center text-sm leading-relaxed text-gray-500">
              Copyright © Project: The Movie 2022. All rights reserved.
              <br />
              Created by{" "}
              <a
                className="text-blue-500 underline transition hover:text-white"
                href="https://www.facebook.com/tqthang91/"
              >
                Thang Tran
              </a>
            </p>
          </div>
        </div>
      </footer>
    </div>
  );
}
