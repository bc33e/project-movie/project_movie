import { Modal } from "antd";
import moment from "moment";
import React from "react";
import { useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import backGroundPage from "../../asset/img/background.jpg";

export default function ButtonBooking(props) {
  const navigate = useNavigate();
  const userInfo = useSelector((state) => state.userReducer.userInfo);
  const [modals, setModals] = useState(false);
  const [checkLogin, setCheckLogin] = useState(true);

  const showModal = () => {
    setModals(true);
  };
  const handleConfirm = () => {
    setModals(false);
  };
  const handleCancel = () => {
    setModals(false);
    setCheckLogin(true);
  };

  let handleConfirmDesktop = () => {
    if (userInfo) {
      window.location.href = `/booking/${props.maLichChieu}`;
    } else {
      setCheckLogin(false);
    }
  };

  let hanldeConfirmMobile = () => {
    if (userInfo) {
      window.location.href = `/bookticket/${props.maLichChieu}`;
    } else {
      navigate("/login");
      Swal.fire({
        icon: "warning",
        title: "Để mua vé, vui lòng đăng nhập",
        showConfirmButton: false,
        timer: 2000,
      });
    }
  };

  let renderBookingInfo = () => {
    return (
      <>
        <div className={checkLogin ? "w-full" : "xl:w-1/2 md:w-3/5"}>
          <div className="flex space-x-4 py-8 border-b justify-center">
            <div
              className="w-1/4 flex items-center justify-center h-44 overflow-hidden cursor-pointer"
              onClick={() => {
                navigate(`/detail/${props.maPhim}`);
              }}
            >
              <img
                className="w-full hover:w-[105%] h-full hover:h-[105%] object-cover rounded duration-200"
                src={props.hinhAnh}
                alt=""
              />
            </div>
            <div className="w-3/4 space-y-3">
              <h2 className="text-lg cursor-pointer text-blue-700 font-medium">
                Tên phim:
              </h2>
              <h1
                onClick={() => {
                  navigate(`/detail/${props.maPhim}`);
                }}
                className="font-bold text-xl cursor-pointer text-center"
              >
                {props.tenPhim}
              </h1>
            </div>
          </div>
          <div className="space-y-5 py-6">
            <div className="flex">
              <div className="w-1/2">
                <div className=" text-blue-700 font-medium">NGÀY CHIẾU: </div>
                <div className="font-bold text-xl">
                  {moment(props.ngayChieuGioChieu).format("DD/MM/YYYY")}
                </div>
              </div>
              <div className="w-1/2">
                <div className=" text-blue-700 font-medium">GIỜ CHIẾU: </div>
                <div className="font-bold text-xl">
                  {moment(props.ngayChieuGioChieu).format("HH:MM")}
                </div>
              </div>
            </div>
            <div className="space-y-1">
              <h1 className="text-blue-700 font-medium">RẠP CHỌN: </h1>
              <p className="text-xl font-bold">{props.tenCumRap}</p>
              <p className="w-max break-normal">{props.diaChiCumRap}</p>
            </div>
          </div>
          <div className="flex items-center justify-center h-12">
            {checkLogin ? (
              <button
                onClick={handleConfirmDesktop}
                className="w-2/3 py-2 hover:py-3 bg-blue-700 text-white  duration-150 rounded-xl"
              >
                Chọn ghế và thanh toán
              </button>
            ) : (
              ""
            )}
          </div>
        </div>
        <div
          style={{ backgroundImage: `url(${backGroundPage})` }}
          className={
            checkLogin
              ? "w-0"
              : "xl:w-2/5 md:w-1/2 space-y-8 xl:h-full md:h-full  duration-700 bg-cover bg-center rounded text-center"
          }
        >
          <h1 className="text-6xl text-center mt-14 text-white font-extrabold ">
            QT Movie
          </h1>
          <p className="text-white text-xl break-normal w-max mx-auto">
            Bạn cần đăng nhập <br /> để mua vé xem phim
          </p>
          <p className="font-bold text-white text-2xl break-normal w-max mx-auto">
            {props.tenPhim}
          </p>
          <button
            onClick={() => {
              navigate("/login");
            }}
            className="w-1/2 py-3 text-lg bg-blue-700 hover:bg-blue-800 text-white hover:text-white rounded duration-150"
          >
            Đăng nhập
          </button>
        </div>
      </>
    );
  };

  return (
    <>
      <button
        className="py-3 px-2 mt-2 mx-2 font-bold text-center border  text-white hover:text-blue-500 bg-blue-500 hover:bg-white duration-300 rounded-lg"
        onClick={showModal}
      >
        {moment(props.ngayChieuGioChieu).format("DD/MM/YYYY ~ HH:MM")}
      </button>
      <Modal
        bodyStyle={{ padding: "35px" }}
        open={modals}
        onConfirm={handleConfirm}
        onCancel={handleCancel}
        footer={null}
        width={checkLogin ? 500 : 900}
        className="duration-700 overflow-hidden"
        centered
      >
        <div className="flex flex-nowrap justify-around items-center overflow-hidden h-[30rem]">
          {renderBookingInfo()}
        </div>
      </Modal>
    </>
  );
}
