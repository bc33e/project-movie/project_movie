import React from "react";
import { useSelector } from "react-redux";
import Lottie from "lottie-react";
import bg_loading from "../../asset/bg_login.json";

export default function Spinner() {
  let { isLoading } = useSelector((state) => {
    return state.spinnerReducer.isLoading;
  });
  return isLoading ? (
    <div className="fixed w-screen h-screen flex justify-center items-center bg-black top-0 left-0 z-50">
      <div className="scale-50 flex w-full h-full items-center justify-center">
        <Lottie animationData={bg_loading} />
      </div>
    </div>
  ) : (
    <></>
  );
}
