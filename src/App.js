import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { routes } from "./Routes/routes";
import Spinner from "./components/Spinner/Spinner";

function App() {
  return (
    <div className="App">
      <Spinner />
      <BrowserRouter>
        <Routes>
          {routes.map(({ path, component }, index) => {
            return <Route key={index} path={path} element={component}></Route>;
          })}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
