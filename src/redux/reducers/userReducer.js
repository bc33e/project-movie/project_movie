import { SET_LOGIN } from "../constants/userConstant";
import { userInfoLocal } from "../../service/local.service";

const initialState = {
  userInfo: userInfoLocal.get(),
};

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_LOGIN: {
      state.userInfo = payload;
      return { ...state };
    }
    default:
      return state;
  }
};
