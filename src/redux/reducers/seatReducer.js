import { CANCEL_SEAT, CHOOSE_SEAT } from "../constants/seatConstant";

const initialState = {
  arrSeat: [],
};
export const seatReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case CHOOSE_SEAT: {
      let cloneArrSeat = [...state.arrSeat];
      let index = cloneArrSeat.findIndex((ghe) => ghe.maGhe === payload.maGhe);
      if (index === -1) {
        cloneArrSeat.push(payload);
      } else {
        cloneArrSeat.splice(index, 1);
      }
      return { ...state, arrSeat: cloneArrSeat };
    }

    case CANCEL_SEAT: {
      let cloneArrSeat = [...state.arrSeat];
      cloneArrSeat = payload;
      return { ...state, arrSeat: cloneArrSeat };
    }

    default:
      return state;
  }
};
