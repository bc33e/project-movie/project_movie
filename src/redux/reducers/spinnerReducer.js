import {
  TURN_ON_LOADING,
  TURN_OFF_LOADING,
} from "../constants/spinnerConstants";

let initialState = {
  isLoading: false,
  count: 0,
};

export const spinnerReducer = (state = initialState, type, payload) => {
  switch (type) {
    case TURN_ON_LOADING: {
      state.count++;
      state.isLoading = true;
      return { ...state };
    }

    case TURN_OFF_LOADING: {
      state.count--;
      if (state.count == 0) {
        state.isLoading = false;
      }
      return { ...state };
    }

    default:
      return state;
  }
};
