import { combineReducers } from "redux";
import { userReducer } from "./userReducer";
import { spinnerReducer } from "./spinnerReducer";
import { seatReducer } from "./seatReducer";

export const rootReducer = combineReducers({
  userReducer,
  spinnerReducer,
  seatReducer,
});
