import { message } from "antd";
import { SET_LOGIN } from "../constants/userConstant";
import { userService } from "../../service/user.service";
import { userInfoLocal } from "../../service/local.service";

export const setLoginActionService = (dataUser, onSuccess) => {
  return (dispatch) => {
    userService
      .postLogin(dataUser)
      .then((res) => {
        console.log("res: ", res);
        //lưu xuống local
        userInfoLocal.set(res.data.content);

        dispatch({
          type: SET_LOGIN,
          payload: res.data.content,
        });
        onSuccess();
      })
      .catch((err) => {
        console.log("err: ", err);
        message.error(err.response.data.content);
      });
  };
};
