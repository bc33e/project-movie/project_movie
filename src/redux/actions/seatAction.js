import { CANCEL_SEAT, CHOOSE_SEAT } from "../constants/seatConstant";

export const handleChooseSeat = (ghe) => {
  return {
    type: CHOOSE_SEAT,
    payload: ghe,
  };
};

export const handleCancelSeat = () => {
  return {
    type: CANCEL_SEAT,
    payload: [],
  };
};
